
# Desafios: Data Engineer

Esta fase do processo seletivo consiste em um desafio incial. Você poderá escolher entre os dois abaixo:

  1. **Categorizando lat/lon**
  2. **Crawleando as estações meteorológicas de São Paulo**

## Opção 1: Categorizando lat/lon

Neste desafio você irá usar a base de dados lat-lon em formato CSV, inclusa neste repositório. Esta base contém vários valores de latitude e longitude no Brasil. Você deverá categorizar estes valores tendo em vista a base de 2010 do IBGE que pode ser encontrada aqui: https://mapas.ibge.gov.br/bases-e-referenciais/bases-cartograficas/malhas-digitais . O desafio compreende você gerar um arquivo em formato CSV contendo o tipo da região (rural ou urbano) e o código do setor censitário para cada id.

Exemplo do CSV fornecido:

| id | latitude          | longitude         |
|----|-------------------|-------------------|
| 1  | -5.6690196916461  | -51.7470072167926 |
| 2  | -2.9192271027714  | -50.5774396997876 |
| 3  | -21.8756447890773 | -46.2293426143005 |
| 4  | -4.38046420458704 | -46.8611718844622 |
| 5  | -9.19121217168868 | -51.5960052940063 |
| 6  | -15.0527414930984 | -49.5029369094409 |
| 7  | -14.1998404329643 | -49.8137575625442 |
| 8  | -14.2994454307482 | -49.1496237716638 |
| 9  | -20.735847171396  | -51.1747123585083 |
| 10 | -11.2237630644813 | -49.7124046627432 |

Exemplo de resultado esperado:

| id | tipo  | cod_setor       |
|----|-------|-----------------|
| 1  | RURAL | 150730040000008 |
| 2  | RURAL | 150580905000028 |
| 3  | RURAL | 311030115000004 |
| 4  | RURAL | 210200205000034 |
| 5  | RURAL | 150730005000021 |
| 6  | RURAL | 521486105000009 |
| 7  | RURAL | 521970405000017 |
| 8  | RURAL | 520470605000009 |
| 9  | RURAL | 350210105000083 |
| 10 | RURAL | 170730605000009 |

### Perguntas

  1. O que você faria caso o arquivo de CSV de entrada contivesse bilhões de linhas e você quisesse obter o resultado em alguns segundos?

  2. Entendendo o desafio de categorizar latitude e longitude como um problema recorrente, como você implementaria um serviço que permitisse uma consulta unitária (obter o tipo e o código do setor censitário dado uma latitude e uma longitude)?

  3. O que você faria se tivesse mais tempo para resolver o desafio?

  4. Como você resolveria esse desafio e/ou as perguntas caso tivesse acesso aos recursos da Amazon Web Services, Azure ou Google Cloud?


### Você deverá entregar

  1. Você deverá criar um repositório Git privado no Bitbucket e compartilhá-lo com o usuário `big-data-brasil`.

  2. Seus códigos a serem avaliados, juntamente com o arquivo de saída, deverão estar contidos na branch `master` do seu repositório.

  3. Você deverá criar um arquivo `Readme.md` contendo a documentação de todos os procedimentos realizados. Neste arquivo deverão estar respondidas as perguntas propostas que poderão ser respondidas separadamente ou em conjunto.

  4. Você será avaliado pela qualidade e efetividade do seu código. Os códigos deverão estar com comentários e bem organizados. Sempre que possível, testes unitários, de integração e linters são bem vindos.
  
  

## Opção 2: Crawleando as estações meteorológicas de São Paulo

Neste desafio você deverá obter os dados das estações meteorológicas de São Paulo através do site: https://www.cgesp.org/v3/estacoes-meteorologicas.jsp . Seu crawler deverá retornar um objeto JSON contendo os seguintes dados de todas as estações meteorológicas nas últimas 24 horas: timestamp (convertido para epoch), chuva, velocidade do vento, direção do vento, temperatura, umidade relativa e pressão. 

Exemplo de resultado esperado do crawler:

    {
      "Penha": [
        {
            "timestamp": 1542632400,
            "chuva": 1.2,
            "vel_vento": 3.01,
            "dir_vento": 8,
            "temp": 17.51,
            "umidadade_rel": 86.92,
            "pressao": 934.55
        },
        {
            "timestamp": 1542628800,
            "chuva": 1,
            "vel_vento": 0,
            "dir_vento": 26,
            "temp": 18.04,
            "umidadade_rel": 86.77,
            "pressao": 933.98
        }, 
        ... 
      ],
      "Perus": [
        {
            "timestamp": 1542632400,
            "chuva": 4,
            "vel_vento": 0.19,
            "dir_vento": 136,
            "temp": 18.22,
            "umidadade_rel": 92.31,
            "pressao": 929.58
        },
        {
            "timestamp": 1542628800,
            "chuva": 3.6,
            "vel_vento": 0,
            "dir_vento": 243,
            "temp": 17.84,
            "umidadade_rel": 93.14,
            "pressao": 929.56
        }, 
        ... 
      ],
      ...
    }

### Perguntas

  1. O que você faria caso quisesse obter essas informações de forma recorrente, ou seja, todo dia?

  2. Como você validaria se as respostas obtidas do crawler estão corretas ou não?

  3. O que você faria se tivesse mais tempo para resolver o desafio?

  4. Como você resolveria esse desafio e/ou as perguntas caso tivesse acesso aos recursos da Amazon Web Services, Azure ou Google Cloud?


### Você deverá entregar

  1. Você deverá criar um repositório Git privado no Bitbucket e compartilhá-lo com o usuário `big-data-brasil`.

  2. Seus códigos a serem avaliados deverão estar contidos na branch `master` do seu repositório.

  3. Você deverá criar um arquivo `Readme.md` contendo a documentação do crawler. Neste arquivo deverão estar respondidas as perguntas propostas que poderão ser respondidas separadamente ou em conjunto.

  4. Você será avaliado pela qualidade e efetividade do seu código. Os códigos deverão estar com comentários e bem organizados. Sempre que possível, testes unitários, de integração e linters são bem vindos.
